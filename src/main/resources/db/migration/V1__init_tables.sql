CREATE SEQUENCE user_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE users (
  id            NUMBER(11, 0) NOT NULL PRIMARY KEY,
  user_id       CHAR(36) NOT NULL UNIQUE,
  user_name     NVARCHAR2(255) NOT NULL UNIQUE,
  password      NVARCHAR2(255) NOT NULL,
  email         NVARCHAR2(255) UNIQUE,
  avatar_path   NVARCHAR2(255),
  avatar_name   NVARCHAR2(255),
  date_of_birth TIMESTAMP,
  address_id    NVARCHAR2(255),
  delete_flg    NUMBER(1, 0) DEFAULT 0 NOT NULL,
  active_flg    NUMBER(1, 0) DEFAULT 0 NOT NULL,
  created_at    TIMESTAMP,
  updated_at    TIMESTAMP
);

CREATE SEQUENCE duoc_quoc_gia_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE duoc_quoc_gia (
  id                   NUMBER(11, 0) NOT NULL PRIMARY KEY,
  duoc_quoc_gia_id     NVARCHAR2(255) NOT NULL,
  ten_thuoc            NVARCHAR2(2000),
  dot_phe_duyet        NVARCHAR2(255),
  so_quyet_dinh        NVARCHAR2(255),
  phe_duyet            NVARCHAR2(255),
  hieu_luc             NVARCHAR2(255),
  so_dang_ky           NVARCHAR2(255),
  hoat_chat            NVARCHAR2(2000),
  phan_loai            NVARCHAR2(2000),
  nong_do              NVARCHAR2(2000),
  ta_duoc              NVARCHAR2(2000),
  bao_che              NVARCHAR2(2000),
  dong_goi             NVARCHAR2(2000),
  tieu_chuan           NVARCHAR2(2000),
  tuoi_tho             NVARCHAR2(255),
  cong_ty_sx           NVARCHAR2(2000),
  cong_ty_sx_code      NVARCHAR2(255),
  nuoc_sx              NVARCHAR2(2000),
  dia_chi_sx           NVARCHAR2(2000),
  cong_ty_dk           NVARCHAR2(2000),
  nuoc_dk              NVARCHAR2(255),
  dia_chi_dk           NVARCHAR2(2000),
  gia_ke_khai          NVARCHAR2(255),
  huong_dan_su_dung    NVARCHAR2(2000),
  huong_dan_su_dung_bn NVARCHAR2(2000),
  nhom_thuoc           NVARCHAR2(2000),
  delete_flg           NUMBER(1, 0) DEFAULT 0,
  created_at           TIMESTAMP,
  updated_at           TIMESTAMP
);

CREATE SEQUENCE thuoc_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE thuoc (
  id                     NUMBER(11, 0) NOT NULL PRIMARY KEY,
  thuoc_id               NVARCHAR2(255) NOT NULL UNIQUE,
  ten_thuoc              NVARCHAR2(2000) NOT NULL,
  ma_vach                NVARCHAR2(2000),
  so_lo_sx               NVARCHAR2(255),
  han_su_dung            TIMESTAMP,
  so_dang_ky             NVARCHAR2(2000),
  hoat_chat_va_ham_luong NVARCHAR2(2000),
  quy_cach_dong_goi      NVARCHAR2(2000),
  nong_do                NVARCHAR2(2000),
  tong_so_luong          NUMBER(11, 0),
  sl_ton_min             NUMBER(11, 0) DEFAULT 0,
  sl_ton_max             NUMBER(11, 0) DEFAULT 9999999999,
  ghi_chu                NVARCHAR2(2000),
  don_vi_co_ban_id       NVARCHAR2(255),
  nhom_thuoc_id          NVARCHAR2(255),
  cong_ty_sx_code        NVARCHAR2(255),
  cong_ty_san_xuat       NVARCHAR2(2000),
  quoc_gia_san_xuat      NVARCHAR2(2000),
  delete_flg             NUMBER(1, 0) DEFAULT 0 NOT NULL,
  created_at             TIMESTAMP,
  updated_at             TIMESTAMP
);

CREATE SEQUENCE gia_thuoc_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE gia_thuoc (
  id            NUMBER(11, 0) NOT NULL PRIMARY KEY,
  gia_thuoc_id  NVARCHAR2(255) NOT NULL UNIQUE,
  thuoc_id      NVARCHAR2(255) NOT NULL,
  don_vi_id     NVARCHAR2(255) NOT NULL,
  gia_nhap      NUMBER(11, 0),
  gia_ban       NUMBER(11, 0),
  ti_le_quy_doi NUMBER(11, 0) NOT NULL,
  is_basic_unit NUMBER(1, 0) NOT NULL,
  delete_flg    NUMBER(1, 0) DEFAULT 0 NOT NULL,
  created_at    TIMESTAMP,
  updated_at    TIMESTAMP
);

CREATE SEQUENCE nhom_thuoc_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE nhom_thuoc (
  id                   NUMBER(11, 0) NOT NULL PRIMARY KEY,
  nhom_thuoc_id        NVARCHAR2(255) NOT NULL UNIQUE,
  drug_group_parent_id NVARCHAR2(255),
  ten_nhom_thuoc       NVARCHAR2(255) NOT NULL UNIQUE,
  drug_group_level     NUMBER(1, 0) DEFAULT 1 NOT NULL,
  delete_flg           NUMBER(1, 0) DEFAULT 0 NOT NULL,
  created_at           TIMESTAMP,
  updated_at           TIMESTAMP
);

CREATE SEQUENCE donvi_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE don_vi (
  id         NUMBER(11, 0) NOT NULL PRIMARY KEY,
  don_vi_id  NVARCHAR2(255) NOT NULL UNIQUE,
  ten_don_vi NVARCHAR2(2000) NOT NULL,
  delete_flg NUMBER(1, 0) DEFAULT 0 NOT NULL,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE SEQUENCE thuoc___donvi_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE thuoc___don_vi (
  id                NUMBER(11, 0) NOT NULL PRIMARY KEY,
  thuoc___don_vi_id NVARCHAR2(255) NOT NULL UNIQUE,
  thuoc_id          NVARCHAR2(255) NOT NULL,
  don_vi_id         NVARCHAR2(255) NOT NULL,
  ti_le_quy_doi     NUMBER(11, 0) NOT NULL,
  delete_flg        NUMBER(1, 0) DEFAULT 0 NOT NULL,
  created_at        TIMESTAMP,
  updated_at        TIMESTAMP
);

CREATE SEQUENCE hoa_don_nhap_kho_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE hoa_don_nhap_kho (
  id                  NUMBER(11, 0) NOT NULL PRIMARY KEY,
  hoa_don_nhap_kho_id NVARCHAR2(255) NOT NULL UNIQUE,
  ten_don_nhap        NVARCHAR2(2000) NOT NULL,
  ma_lo_hang          NVARCHAR2(255),
  ngay_nhap           TIMESTAMP,
  ghi_chu             NVARCHAR2(2000),
  nha_cung_cap        NVARCHAR2(2000),
  delete_flg          NUMBER(1, 0) DEFAULT 0 NOT NULL,
  active_flg          NUMBER(1, 0) DEFAULT 0 NOT NULL,
  created_at          TIMESTAMP,
  updated_at          TIMESTAMP
);

CREATE SEQUENCE hoa_don_nhap_kho___thuoc_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE hoa_don_nhap_kho___thuoc (
  id                          NUMBER(11, 0) NOT NULL PRIMARY KEY,
  hoa_don_nhap_kho___thuoc_id NVARCHAR2(255) NOT NULL UNIQUE,
  hoa_don_nhap_kho_id         NVARCHAR2(255) NOT NULL,
  thuoc_id                    NVARCHAR2(255) NOT NULL,
  don_vi_id                   NVARCHAR2(255) NOT NULL,
  so_luong                    NUMBER(11, 0),
  han_su_dung                 TIMESTAMP,
  delete_flg                  NUMBER(1, 0) DEFAULT 0 NOT NULL,
  created_at                  TIMESTAMP,
  updated_at                  TIMESTAMP
);

CREATE SEQUENCE khach_hang_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE khach_hang (
  id             NUMBER(11, 0) NOT NULL PRIMARY KEY,
  khach_hang_id  NVARCHAR2(255) NOT NULL UNIQUE,
  ten_khach_hang NVARCHAR2(500),
  so_dien_thoai  CHAR(11),
  dia_chi        NVARCHAR2(2000),
  ghi_chu        NVARCHAR2(2000),
  delete_flg     NUMBER(1, 0) DEFAULT 0 NOT NULL,
  created_at     TIMESTAMP,
  updated_at     TIMESTAMP
);

CREATE SEQUENCE hoa_don_ban_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE hoa_don_ban (
  id                NUMBER(11, 0) NOT NULL PRIMARY KEY,
  hoa_don_ban_id    NVARCHAR2(255) NOT NULL UNIQUE,
  khach_hang_id     NVARCHAR2(255) NOT NULL,
  ten_hoa_don       NVARCHAR2(500) NOT NULL,
  ngay_xuat_hoa_don TIMESTAMP,
  tong_tien         NUMBER(11, 0),
  ghi_chu           NVARCHAR2(2000),
  delete_flg        NUMBER(1, 0) DEFAULT 0 NOT NULL,
  created_at        TIMESTAMP,
  updated_at        TIMESTAMP
);

CREATE SEQUENCE hoa_don_ban___thuoc_seq INCREMENT BY 1 START WITH 1 CACHE 2;

CREATE TABLE hoa_don_ban___thuoc (
  id                     NUMBER(11, 0) NOT NULL PRIMARY KEY,
  hoa_don_ban___thuoc_id NVARCHAR2(255) NOT NULL UNIQUE,
  hoa_don_ban_id         NVARCHAR2(255) NOT NULL,
  thuoc_id               NVARCHAR2(255) NOT NULL,
  don_vi_id              NVARCHAR2(255) NOT NULL,
  so_luong               NUMBER(11, 0),
  ghi_chu                NVARCHAR2(2000),
  delete_flg             NUMBER(1, 0) DEFAULT 0 NOT NULL,
  created_at             TIMESTAMP,
  updated_at             TIMESTAMP
);
/* DATA */
INSERT INTO users (
  id,
  user_id,
  user_name,
  password,
  email,
  avatar_path,
  avatar_name,
  date_of_birth,
  address_id,
  created_at,
  updated_at
) VALUES (
  user_seq.NEXTVAL,
  '65f575a3-9a10-4058-b314-b7ae8ff84c4d',
  'hoanguser1',
  'p0p0p0',
  'hoangdp1993@gmail.com',
  NULL,
  NULL,
  sysdate,
  'Ha Noi',
  sysdate,
  sysdate
);