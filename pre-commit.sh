#!/bin/bash
set -e
echo "Starting format code..."
# shellcheck disable=SC2046
java -jar ./assets/google-java-format-1.15.0-all-deps.jar --replace $(find . -type f -name "*.java" | grep ".*/src/.*java")
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  echo "OS: $OSTYPE"
  GIT='/usr/bin/git'
elif [[ "$OSTYPE" == "darwin"* ]]; then
  echo "OS: $OSTYPE"
elif [[ "$OSTYPE" == "cygwin" ]]; then
  # POSIX compatibility layer and Linux environment emulation for Windows
  echo "OS: $OSTYPE"
elif [[ "$OSTYPE" == "msys" ]]; then
  GIT='git'
  # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
  echo "OS: $OSTYPE"
elif [[ "$OSTYPE" == "win32" ]]; then
  GIT='git --git-dir='$PWD'/.git'
elif [[ "$OSTYPE" == "freebsd"* ]]; then
  # ...
  echo "OS: $OSTYPE"
else
  # Unknown.
  echo "OS: $OSTYPE"
fi
$GIT add .
echo "Process done!"