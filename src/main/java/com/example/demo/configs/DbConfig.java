package com.example.demo.configs;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.integration.IntegrationDataSourceScriptDatabaseInitializer;
import org.springframework.boot.sql.init.DatabaseInitializationSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DbConfig {
	@Bean
	public IntegrationDataSourceScriptDatabaseInitializer customIntegrationDataSourceInitializer(
			final DataSource dataSource) {
		return new IntegrationDataSourceScriptDatabaseInitializer(dataSource, new DatabaseInitializationSettings());
	}
}